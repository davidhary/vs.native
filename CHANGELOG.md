# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [x.x.7616] - 2020-11-06
* Moved from the [Share Repository](https://www.bitbucket.org/davidhary/vs.share)

\(C\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
```

Imports System.Runtime.InteropServices
Imports System.Security
Partial Friend Class NativeMethods

#Region " WINDOWS STRUCTURES AND ENUMS "

    <Flags>
    Public Enum TCHITTESTFLAGS
        TCHT_NOWHERE = 1
        TCHT_ONITEMICON = 2
        TCHT_ONITEMLABEL = 4
        TCHT_ONITEM = TCHT_ONITEMICON Or TCHT_ONITEMLABEL
    End Enum

    <StructLayout(LayoutKind.Sequential)>
    Public Structure TCHITTESTINFO

        Public Sub New(location As Point)
            pt = location
            flags = TCHITTESTFLAGS.TCHT_ONITEM
        End Sub

        Public pt As Point
        Public flags As TCHITTESTFLAGS
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=4)>
    Public Structure PAINTSTRUCT
        ' A handle to the display DC to use for painting. 
        Public hdc As IntPtr
        ' Indicates whether the background should be erased. 
        Public fErase As Integer
        ' A RECT structure that specifies the upper left and lower right 
        ' corners of the rectangle in which the painting is requested, 
        Public rcPaint As RECT
        ' Reserved; used internally by the system. 
        Public fRestore As Integer
        ' Reserved; used internally by the system. 
        Public fIncUpdate As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
        Public rgbReserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure RECT
        ' The x-coordinate of the upper-left corner of the rectangle. 
        Public left As Integer
        ' The y-coordinate of the upper-left corner of the rectangle. 
        Public top As Integer
        ' The x-coordinate of the lower-right corner of the rectangle. 
        Public right As Integer
        ' The y-coordinate of the lower-right corner of the rectangle. 
        Public bottom As Integer

        Public Sub New(left As Integer, top As Integer, right As Integer, bottom As Integer)
            Me.left = left
            Me.top = top
            Me.right = right
            Me.bottom = bottom
        End Sub

        Public Sub New(r As Rectangle)
            Me.left = r.Left
            Me.top = r.Top
            Me.right = r.Right
            Me.bottom = r.Bottom
        End Sub

        Public Shared Function FromXYWH(x As Integer, y As Integer, width As Integer, height As Integer) As RECT
            Return New RECT(x, y, x + width, y + height)
        End Function

        Public Shared Function FromIntPtr(ptr As IntPtr) As RECT
            Dim rect As RECT = CType(Marshal.PtrToStructure(ptr, GetType(RECT)), RECT)
            Return rect
        End Function

        Public ReadOnly Property Size() As Size
            Get
                Return New Size(Me.right - Me.left, Me.bottom - Me.top)
            End Get
        End Property
    End Structure

#End Region

#Region " WINDOWS CONSTANTS "

    Public Const WM_GETTABRECT As Integer = &H130A

    Public Const WS_EX_TRANSPARENT As Integer = &H20

    Public Const WM_SETFONT As Integer = &H30

    Public Const WM_FONTCHANGE As Integer = &H1D

    Public Const WM_HSCROLL As Integer = &H114

    Public Const TCM_HITTEST As Integer = &H130D

    ''' <summary> The windows message paint. Sent when the system makes a request to paint (a portion of) a window
    ''' </summary>
    Public Const WM_PAINT As Integer = &HF

    Public Const WS_EX_LAYOUTRTL As Integer = &H400000

    Public Const WS_EX_NOINHERITLAYOUT As Integer = &H100000

#End Region

#Region " CONTENT ALIGNMENT "

    Public Const AnyRightAlign As ContentAlignment = ContentAlignment.BottomRight Or ContentAlignment.MiddleRight Or ContentAlignment.TopRight

    Public Const AnyLeftAlign As ContentAlignment = ContentAlignment.BottomLeft Or ContentAlignment.MiddleLeft Or ContentAlignment.TopLeft

    Public Const AnyTopAlign As ContentAlignment = ContentAlignment.TopRight Or ContentAlignment.TopCenter Or ContentAlignment.TopLeft

    Public Const AnyBottomAlign As ContentAlignment = ContentAlignment.BottomRight Or ContentAlignment.BottomCenter Or ContentAlignment.BottomLeft

    Public Const AnyMiddleAlign As ContentAlignment = ContentAlignment.MiddleRight Or ContentAlignment.MiddleCenter Or ContentAlignment.MiddleLeft

    Public Const AnyCenterAlign As ContentAlignment = ContentAlignment.BottomCenter Or ContentAlignment.MiddleCenter Or ContentAlignment.TopCenter

#End Region

End Class

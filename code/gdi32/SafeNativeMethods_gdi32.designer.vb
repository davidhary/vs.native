Imports System
Imports System.Drawing
Imports System.Runtime.InteropServices

''' <summary>
''' A safe native GDI32 methods. P/Invoke method declarations which are harmless for any code to
''' call.
''' </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-06-18 </para></remarks>
Public NotInheritable Class SafeNativeMethods

    Private Sub New()
    End Sub

    Public Enum DrawingMode
        None
        R2_NOTXORPEN = 10
    End Enum

    <DllImport("gdi32.dll")>
    Friend Shared Function Rectangle(ByVal hDC As IntPtr, ByVal left As Integer, ByVal top As Integer, ByVal right As Integer, ByVal bottom As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("gdi32.dll")>
    Friend Shared Function SetROP2(ByVal hDC As IntPtr, ByVal fnDrawMode As Integer) As Integer
    End Function

    <DllImport("gdi32.dll")>
    Friend Shared Function MoveToEx(ByVal hDC As IntPtr, ByVal x As Integer, ByVal y As Integer, ByRef p As Point) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("gdi32.dll")>
    Friend Shared Function LineTo(ByVal hdc As IntPtr, ByVal x As Integer, ByVal y As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("gdi32.dll")>
    Friend Shared Function CreatePen(ByVal fnPenStyle As Integer, ByVal nWidth As Integer, ByVal crColor As Integer) As IntPtr
    End Function

    ''' <summary> Select object. </summary>
    ''' <remarks>
    ''' Selects an object into the specified device context (DC). The new object replaces the
    ''' previous object of the same type.
    ''' </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="hObj"> The object. </param>
    ''' <returns> An IntPtr. </returns>
    <DllImport("gdi32.dll")>
    Friend Shared Function SelectObject(ByVal hDC As IntPtr, ByVal hObj As IntPtr) As IntPtr
    End Function

    ''' <summary> Deletes the object described by hObject. </summary>
    ''' <remarks>
    ''' Deletes a logical pen, brush, font, bitmap, region, or palette, freeing all system resources
    ''' associated with the object. After the object has been deleted, the specified handle is no
    ''' longer valid.
    ''' </remarks>
    ''' <param name="hObj"> The object. </param>
    <DllImport("gdi32.dll")>
    Friend Shared Function DeleteObject(ByVal hObj As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private Shared nullPoint As New Point(0, 0)

    ''' <summary> ARGB from .NET to a GDI32 RGB. </summary>
    ''' <param name="rgb"> The RGB. </param>
    ''' <returns> An Integer. </returns>
    Private Shared Function ArgbToRGB(ByVal rgb As Integer) As Integer
        Return ((rgb >> 16 And &HFF) Or (rgb And &HFF00) Or (rgb << 16 And &HFF0000))
    End Function

    ''' <summary> Draw exclusive-or rectangle. </summary>
    ''' <param name="graphics">  The graphics. </param>
    ''' <param name="pen">       The pen. </param>
    ''' <param name="rectangle"> The rectangle. </param>
    Friend Shared Sub DrawXorRectangle(ByVal graphics As Graphics, ByVal pen As Pen, ByVal rectangle As Rectangle)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If pen Is Nothing Then Throw New ArgumentNullException(NameOf(pen))
        Dim hDC As IntPtr = graphics.GetHdc()
        Dim hPen As IntPtr = CreatePen(CInt(pen.DashStyle), CInt(pen.Width), ArgbToRGB(pen.Color.ToArgb()))
        SelectObject(hDC, hPen)
        SetROP2(hDC, CInt(DrawingMode.R2_NOTXORPEN))
        SafeNativeMethods.Rectangle(hDC, rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom)
        DeleteObject(hPen)
        graphics.ReleaseHdc(hDC)
    End Sub

    ''' <summary> Draw exclusive-or line. </summary>
    ''' <param name="graphics"> The graphics. </param>
    ''' <param name="pen">      The pen. </param>
    ''' <param name="x1">       The first x value. </param>
    ''' <param name="y1">       The first y value. </param>
    ''' <param name="x2">       The second x value. </param>
    ''' <param name="y2">       The second y value. </param>
    Friend Shared Sub DrawXorLine(ByVal graphics As Graphics, ByVal pen As Pen, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If pen Is Nothing Then Throw New ArgumentNullException(NameOf(pen))
        Dim hDC As IntPtr = graphics.GetHdc()
        Dim hPen As IntPtr = CreatePen(CInt(pen.DashStyle), CInt(pen.Width), ArgbToRGB(pen.Color.ToArgb()))
        SelectObject(hDC, hPen)
        SetROP2(hDC, CInt(DrawingMode.R2_NOTXORPEN))
        MoveToEx(hDC, x1, y1, nullPoint)
        LineTo(hDC, x2, y2)
        DeleteObject(hPen)
        graphics.ReleaseHdc(hDC)
    End Sub
End Class



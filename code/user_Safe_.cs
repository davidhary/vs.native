using System.Runtime.InteropServices;
using System;

namespace UpgradeSolution1Support.PInvoke.SafeNative
{
	public static class user
	{

		public static short DestroyCursor(short hCursor)
		{
			return UpgradeSolution1Support.PInvoke.UnsafeNative.user.DestroyCursor(hCursor);
		}
		public static short LoadCursor(short hInstance, string lpCursorName)
		{
			short result2 = 0;
			IntPtr tmpPtr = Marshal.StringToHGlobalAnsi(lpCursorName);
			try
			{
				result2 = UpgradeSolution1Support.PInvoke.UnsafeNative.user.LoadCursor(hInstance, tmpPtr);
				lpCursorName = Marshal.PtrToStringAnsi(tmpPtr);
			}
			finally
			{
				Marshal.FreeHGlobal(tmpPtr);
			}
			return result2;
		}
	}
}
Partial Friend NotInheritable Class SafeNativeMethods
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub FreeLibrary(ByVal libraryHandle As IntPtr)
        UnsafeNativeMethods.FreeLibrary(libraryHandle)
    End Sub
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Function LoadLibrary(ByVal fileName As String) As IntPtr
        Return UnsafeNativeMethods.LoadLibrary(fileName)
    End Function

End Class

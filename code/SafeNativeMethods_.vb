''' <summary>
''' A safe native methods. Wraps P/Invoke method declarations which are harmless for any code to
''' call.
''' </summary>
''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2019-06-18 </para></remarks>
Partial Friend NotInheritable Class SafeNativeMethods

#Region " CONSTRUCTION "
    Private Sub New()
    End Sub
#End Region

End Class

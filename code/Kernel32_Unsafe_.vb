﻿Imports System
Imports System.IO
Imports System.Runtime.InteropServices

Partial Friend NotInheritable Class UnsafeNativeMethods

#Region " LIBRARY "

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Friend Shared Function LoadLibrary(ByVal libname As String) As IntPtr
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("kernel32.dll", SetLastError:=True, EntryPoint:="FreeLibrary")>
    Friend Shared Function FreeLibrary(ByVal hModule As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private Shared ReadOnly _nativeLoadLock As New Object()
    Private Shared _nativeAssembliesLoaded As Boolean
    ''' <summary>
    ''' Loads the required native assemblies for the current architecture (x86 or x64)
    ''' </summary>
    ''' <param name="rootApplicationPath">
    ''' Root path of the current application. Use Server.MapPath(".") for ASP.NET applications
    ''' and AppDomain.CurrentDomain.BaseDirectory for desktop applications.
    ''' </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Public Shared Sub LoadNativeAssemblies(ByVal rootApplicationPath As String)
        If _nativeAssembliesLoaded Then
            Return
        End If
        SyncLock _nativeLoadLock
            If Not _nativeAssembliesLoaded Then
                Dim nativeBinaryPath As String = If(IntPtr.Size > 4, System.IO.Path.Combine(rootApplicationPath, "x64\"), System.IO.Path.Combine(rootApplicationPath, "x86\"))
                Console.Write("(from: " & nativeBinaryPath & ")...")
                LoadNativeAssembly(nativeBinaryPath, "msvcr120.dll")
                LoadNativeAssembly(nativeBinaryPath, "SqlServerSpatial140.dll")
                _nativeAssembliesLoaded = True
            End If
        End SyncLock
    End Sub

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Sub LoadNativeAssembly(ByVal nativeBinaryPath As String, ByVal assemblyName As String)
        Dim path As String = System.IO.Path.Combine(nativeBinaryPath, assemblyName)
        Dim ptr As IntPtr = LoadLibrary(path)
        If ptr = IntPtr.Zero Then
            Throw New InvalidOperationException(String.Format("Error loading {0} (Error Code: {1})", assemblyName, Marshal.GetLastWin32Error()))
        End If
    End Sub

#End Region

End Class

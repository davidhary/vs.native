Imports System
Imports System.Runtime.InteropServices
Namespace UnsafeNative
    <CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2118:ReviewSuppressUnmanagedCodeSecurityUsage")>
    <System.Security.SuppressUnmanagedCodeSecurity>
    Friend Module Kernel
        Declare Sub FreeLibrary Lib "Kernel" (ByVal libraryModuleHandle As Short)
        Declare Function LoadLibrary Lib "Kernel" (ByVal fileName As String) As Short
    End Module
End Namespace
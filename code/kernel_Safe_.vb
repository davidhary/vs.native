Imports System
Imports System.Runtime.InteropServices
Namespace SafeNative
    Friend Module Kernel
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub FreeLibrary(ByVal hLibModule As Short)
            UnsafeNative.Kernel.FreeLibrary(hLibModule)
        End Sub
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function LoadLibrary(ByVal fileName As String) As Short
            Return UnsafeNative.Kernel.LoadLibrary(fileName)
        End Function
    End Module
End Namespace
' This class is excluded form code analysis by the Genergate Code attribute.
Imports System.Drawing
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions
Imports System.Windows.Forms

<GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")>
<SecurityPermission(SecurityAction.Assert, Flags:=SecurityPermissionFlag.UnmanagedCode)>
Partial Friend NotInheritable Class UnsafeNativeMethods

    ''' <summary> structure for GetScrollInfo call (used by GetScrollBarInfo) </summary>
    <StructLayout(LayoutKind.Sequential)>
    Public Structure ScrollInfo
        '   INPUT: What information to get
        Public SizeOfStructure As Integer 'infrastructure
        Public ScrollBarMask As Integer 'tells which fields below to retrieve information for
        '   OUTPUT: Scrollbar information
        Public MinimumScrollPosition As Integer
        Public MaximumScrollPosition As Integer
        Public PageSize As Integer
        Public Position As Integer
        Public TrackPosition As Integer
    End Structure

#Region " WINDOWS STRUCTURES AND ENUMS "

    <Flags>
    Public Enum TCHITTESTFLAGS
        TCHT_NOWHERE = 1
        TCHT_ONITEMICON = 2
        TCHT_ONITEMLABEL = 4
        TCHT_ONITEM = TCHT_ONITEMICON Or TCHT_ONITEMLABEL
    End Enum

    <StructLayout(LayoutKind.Sequential)>
    Public Structure TCHITTESTINFO
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Sub New(location As Point)
            Point = location
            Flags = TCHITTESTFLAGS.TCHT_ONITEM
        End Sub
        Public Point As Point
        Public Flags As TCHITTESTFLAGS
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=4)>
    Public Structure PAINTSTRUCT

        ' A handle to the display DC to use for painting. 
        Public Hdc As IntPtr

        ' Indicates whether the background should be erased. 
        Public fErase As Integer

        ' A RECT structure that specifies the upper left and lower right 
        ' corners of the rectangle in which the painting is requested, 
        Public rcPaint As RECT

        ' Reserved; used internally by the system. 
        Public fRestore As Integer

        ' Reserved; used internally by the system. 
        Public fIncUpdate As Integer

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=32)>
        Public RgbReserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Public Structure RECT
        ' The x-coordinate of the upper-left corner of the rectangle. 
        Public Left As Integer
        ' The y-coordinate of the upper-left corner of the rectangle. 
        Public Top As Integer
        ' The x-coordinate of the lower-right corner of the rectangle. 
        Public Right As Integer
        ' The y-coordinate of the lower-right corner of the rectangle. 
        Public Bottom As Integer

        Public Sub New(left As Integer, top As Integer, right As Integer, bottom As Integer)
            Me.Left = left
            Me.Top = top
            Me.Right = right
            Me.Bottom = bottom
        End Sub

        Public Sub New(r As Rectangle)
            Me.Left = r.Left
            Me.Top = r.Top
            Me.Right = r.Right
            Me.Bottom = r.Bottom
        End Sub

        Public Shared Function FromXYWH(x As Integer, y As Integer, width As Integer, height As Integer) As RECT
            Return New RECT(x, y, x + width, y + height)
        End Function

        Public Shared Function FromIntPtr(ptr As IntPtr) As RECT
            Dim rect As RECT = CType(Marshal.PtrToStructure(ptr, GetType(RECT)), RECT)
            Return rect
        End Function

        Public ReadOnly Property Size() As Size
            Get
                Return New Size(Me.Right - Me.Left, Me.Bottom - Me.Top)
            End Get
        End Property
    End Structure

#End Region

End Class


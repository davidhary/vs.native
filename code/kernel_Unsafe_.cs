using System.Runtime.InteropServices;
using System;

namespace UpgradeSolution1Support.PInvoke.UnsafeNative
{
	[System.Security.SuppressUnmanagedCodeSecurity]
	public static class kernel
	{

		[DllImport("Kernel.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static void FreeLibrary(short hLibModule);
		[DllImport("Kernel.dll", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		extern public static short LoadLibrary([MarshalAs(UnmanagedType.VBByRefStr)] ref string lpLibFileName);
	}
}
﻿Imports System
Imports System.Drawing
Imports System.Runtime.InteropServices

Partial Friend NotInheritable Class UnsafeNativeMethods

    Public Enum DrawingMode
        None
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="NOTXORPEN")>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")>
        R2_NOTXORPEN = 10
    End Enum

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="h")>
    <DllImport("gdi32.dll")>
    Friend Shared Function Rectangle(ByVal hDC As IntPtr, ByVal left As Integer, ByVal top As Integer, ByVal right As Integer, ByVal bottom As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="h")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="fn")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="ROP")>
    <DllImport("gdi32.dll")>
    Friend Shared Function SetROP2(ByVal hDC As IntPtr, ByVal fnDrawMode As Integer) As Integer
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="h")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="p")>
        <DllImport("gdi32.dll")>
    Friend Shared Function MoveToEx(ByVal hDC As IntPtr, ByVal x As Integer, ByVal y As Integer, ByRef p As Point) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("gdi32.dll")>
    Friend Shared Function LineTo(ByVal hdc As IntPtr, ByVal x As Integer, ByVal y As Integer) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="n")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="fn")>
    <DllImport("gdi32.dll")>
    Friend Shared Function CreatePen(ByVal fnPenStyle As Integer, ByVal nWidth As Integer, ByVal crColor As Integer) As IntPtr
    End Function

    ''' <summary> Select object. </summary>
    ''' <remarks>
    ''' Selects an object into the specified device context (DC). The new object replaces the
    ''' previous object of the same type.
    ''' </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="hObj"> The object. </param>
    ''' <returns> An IntPtr. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="h")>
    <DllImport("gdi32.dll")>
    Friend Shared Function SelectObject(ByVal hDC As IntPtr, ByVal hObj As IntPtr) As IntPtr
    End Function

    ''' <summary> Select object. </summary>
    ''' <remarks>
    ''' Selects an object into the specified device context (DC). The new object replaces the
    ''' previous object of the same type.
    ''' </remarks>
    ''' <param name="hDC">     The device-context. </param>
    ''' <param name="hObject"> The object. </param>
    ''' <returns> An IntPtr. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function SelectObject(ByVal hDC As HandleRef, ByVal hObject As HandleRef) As IntPtr
    End Function

    ''' <summary> Deletes the object described by hObject. </summary>
    ''' <remarks>
    ''' Deletes a logical pen, brush, font, bitmap, region, or palette, freeing all system resources
    ''' associated with the object. After the object has been deleted, the specified handle is no
    ''' longer valid.
    ''' </remarks>
    ''' <param name="hObj"> The object. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="h")>
    <DllImport("gdi32.dll")>
    Friend Shared Function DeleteObject(ByVal hObj As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    ''' <summary> Deletes the object described by hObject. </summary>
    ''' <remarks>
    ''' Deletes a logical pen, brush, font, bitmap, region, or palette, freeing all system resources
    ''' associated with the object. After the object has been deleted, the specified handle is no
    ''' longer valid.
    ''' </remarks>
    ''' <param name="hObject"> The object. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, ExactSpelling:=False)>
    Friend Shared Function DeleteObject(ByVal hObject As HandleRef) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private Shared nullPoint As New Point(0, 0)

    ''' <summary> ARGB from .NET to a GDI32 RGB. </summary>
    ''' <param name="rgb"> The RGB. </param>
    ''' <returns> An Integer. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Shared Function ArgbToRGB(ByVal rgb As Integer) As Integer
        Return ((rgb >> 16 And &HFF) Or (rgb And &HFF00) Or (rgb << 16 And &HFF0000))
    End Function

    ''' <summary> Draw exclusive-or rectangle. </summary>
    ''' <param name="graphics">  The graphics. </param>
    ''' <param name="pen">       The pen. </param>
    ''' <param name="rectangle"> The rectangle. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId:="isr.Visuals.RealTimeChart.SafeNativeMethods.SetROP2(System.IntPtr,System.Int32)")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId:="isr.Visuals.RealTimeChart.GDI32.SetROP2(System.IntPtr,System.Int32)")>
    Friend Shared Sub DrawXorRectangle(ByVal graphics As Graphics, ByVal pen As Pen, ByVal rectangle As Rectangle)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If pen Is Nothing Then Throw New ArgumentNullException(NameOf(pen))
        Dim hDC As IntPtr = graphics.GetHdc()
        Dim hPen As IntPtr = CreatePen(CInt(pen.DashStyle), CInt(pen.Width), ArgbToRGB(pen.Color.ToArgb()))
        SelectObject(hDC, hPen)
        SetROP2(hDC, CInt(DrawingMode.R2_NOTXORPEN))
        UnsafeNativeMethods.Rectangle(hDC, rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom)
        DeleteObject(hPen)
        graphics.ReleaseHdc(hDC)
    End Sub

    ''' <summary> Draw exclusive-or line. </summary>
    ''' <param name="graphics"> The graphics. </param>
    ''' <param name="pen">      The pen. </param>
    ''' <param name="x1">       The first x value. </param>
    ''' <param name="y1">       The first y value. </param>
    ''' <param name="x2">       The second x value. </param>
    ''' <param name="y2">       The second y value. </param>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId:="isr.Visuals.RealTimeChart.SafeNativeMethods.SetROP2(System.IntPtr,System.Int32)")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId:="isr.Visuals.RealTimeChart.GDI32.SetROP2(System.IntPtr,System.Int32)")>
    Friend Shared Sub DrawXorLine(ByVal graphics As Graphics, ByVal pen As Pen, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer)
        If graphics Is Nothing Then Throw New ArgumentNullException(NameOf(graphics))
        If pen Is Nothing Then Throw New ArgumentNullException(NameOf(pen))
        Dim hDC As IntPtr = graphics.GetHdc()
        Dim hPen As IntPtr = CreatePen(CInt(pen.DashStyle), CInt(pen.Width), ArgbToRGB(pen.Color.ToArgb()))
        SelectObject(hDC, hPen)
        SetROP2(hDC, CInt(DrawingMode.R2_NOTXORPEN))
        MoveToEx(hDC, x1, y1, nullPoint)
        LineTo(hDC, x2, y2)
        DeleteObject(hPen)
        graphics.ReleaseHdc(hDC)
    End Sub

    ''' <summary> Gets text metrics. </summary>
    ''' <remarks> Fills the specified buffer with the metrics for the currently selected font. </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="lptm"> [in,out] The metrics. </param>
    ''' <returns> The text metrics. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Friend Shared Function GetTextMetrics(ByVal hDC As HandleRef, ByRef lptm As UnsafeNativeMethods.TEXTMETRIC) As Integer
        If Marshal.SystemDefaultCharSize <> 1 Then
            ' Handle Unicode
            Return UnsafeNativeMethods.GetTextMetricsW(hDC, lptm)
        End If

        ' Handle ANSI; call GetTextMetricsA and translate to Unicode structure
        Dim tEXTMETRICA As New UnsafeNativeMethods.TEXTMETRICA()
        Dim result As Integer = UnsafeNativeMethods.GetTextMetricsA(hDC, tEXTMETRICA)

        lptm.tmHeight = tEXTMETRICA.tmHeight
        lptm.tmAscent = tEXTMETRICA.tmAscent
        lptm.tmDescent = tEXTMETRICA.tmDescent
        lptm.tmInternalLeading = tEXTMETRICA.tmInternalLeading
        lptm.tmExternalLeading = tEXTMETRICA.tmExternalLeading
        lptm.tmAveCharWidth = tEXTMETRICA.tmAveCharWidth
        lptm.tmMaxCharWidth = tEXTMETRICA.tmMaxCharWidth
        lptm.tmWeight = tEXTMETRICA.tmWeight
        lptm.tmOverhang = tEXTMETRICA.tmOverhang
        lptm.tmDigitizedAspectX = tEXTMETRICA.tmDigitizedAspectX
        lptm.tmDigitizedAspectY = tEXTMETRICA.tmDigitizedAspectY
        lptm.tmFirstChar = Convert.ToChar(tEXTMETRICA.tmFirstChar)
        lptm.tmLastChar = Convert.ToChar(tEXTMETRICA.tmLastChar)
        lptm.tmDefaultChar = Convert.ToChar(tEXTMETRICA.tmDefaultChar)
        lptm.tmBreakChar = Convert.ToChar(tEXTMETRICA.tmBreakChar)
        lptm.tmItalic = tEXTMETRICA.tmItalic
        lptm.tmUnderlined = tEXTMETRICA.tmUnderlined
        lptm.tmStruckOut = tEXTMETRICA.tmStruckOut
        lptm.tmPitchAndFamily = tEXTMETRICA.tmPitchAndFamily
        lptm.tmCharSet = tEXTMETRICA.tmCharSet

        Return result
    End Function

    ''' <summary> Gets text metrics a. </summary>
    ''' <remarks> Fills the specified buffer with the metrics for the currently selected font. This is the ANSI version of the function  </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="lptm"> [in,out] The metrics. </param>
    ''' <returns> The text metrics a. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("gdi32.dll", CharSet:=CharSet.Ansi, ExactSpelling:=False)>
    Friend Shared Function GetTextMetricsA(ByVal hDC As HandleRef, ByRef lptm As UnsafeNativeMethods.TEXTMETRICA) As Integer
    End Function

    ''' <summary> Gets text metrics w. </summary>
    ''' <remarks>
    ''' Fills the specified buffer with the metrics for the currently selected font. This is the
    ''' Unicode version of the function.
    ''' </remarks>
    ''' <param name="hDC">  The device-context. </param>
    ''' <param name="lptm"> [in,out] The metrics. </param>
    ''' <returns> The text metrics w. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("gdi32.dll", CharSet:=CharSet.Unicode, ExactSpelling:=False)>
    Friend Shared Function GetTextMetricsW(ByVal hDC As HandleRef, ByRef lptm As UnsafeNativeMethods.TEXTMETRIC) As Integer
    End Function

    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <DllImport("Gdi32.dll", EntryPoint:="CreateRoundRectRgn")>
    Friend Shared Function CreateRoundRectRgn(ByVal nLeftRect As Integer, ByVal nTopRect As Integer,
                                              ByVal nRightRect As Integer, ByVal nBottomRect As Integer,
                                              ByVal nWidthEllipse As Integer, ByVal nHeightEllipse As Integer) As IntPtr
    End Function

#Region " STRUCTURES "

    '  Contains basic information about a physical font. This is the Unicode version of the structure. 
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Unicode)>
    Public Structure TEXTMETRIC
        ' The height (ascent + descent) of characters. 
        Public tmHeight As Integer

        ' The ascent (units above the base line) of characters. 
        Public tmAscent As Integer

        ' The descent (units below the base line) of characters. 
        Public tmDescent As Integer

        ' The amount of leading (space) inside the bounds set by the tmHeight member. 
        Public tmInternalLeading As Integer

        ' The amount of extra leading (space) that the application adds between rows. 
        Public tmExternalLeading As Integer

        ' The average width of characters in the font (generally defined as the width of the letter x). 
        Public tmAveCharWidth As Integer

        ' The width of the widest character in the font. 
        Public tmMaxCharWidth As Integer

        ' The weight of the font. 
        Public tmWeight As Integer

        ' The extra width per string that may be added to some synthesized fonts. 
        Public tmOverhang As Integer

        ' The horizontal aspect of the device for which the font was designed. 
        Public tmDigitizedAspectX As Integer

        ' The vertical aspect of the device for which the font was designed. 
        Public tmDigitizedAspectY As Integer

        ' The value of the first character defined in the font. 
        Public tmFirstChar As Char

        ' The value of the last character defined in the font. 
        Public tmLastChar As Char

        ' The value of the character to be substituted for characters not in the font. 
        Public tmDefaultChar As Char

        ' The value of the character that will be used to define word breaks for text justification. 
        Public tmBreakChar As Char

        ' Specifies an italic font if it is nonzero. 
        Public tmItalic As Byte

        ' Specifies an underlined font if it is nonzero. 
        Public tmUnderlined As Byte

        ' A strikeout font if it is nonzero. 
        Public tmStruckOut As Byte

        ' Specifies information about the pitch, the technology, and the family of a physical font. 
        Public tmPitchAndFamily As Byte

        ' The character set of the font. 
        Public tmCharSet As Byte
    End Structure

    ' Contains basic information about a physical font. This is the ANSI version of the structure. 
    <System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet:=System.Runtime.InteropServices.CharSet.Ansi)>
    Public Structure TEXTMETRICA
        ' The height (ascent + descent) of characters. 
        Public tmHeight As Integer

        ' The ascent (units above the base line) of characters. 
        Public tmAscent As Integer

        ' The descent (units below the base line) of characters. 
        Public tmDescent As Integer

        ' The amount of leading (space) inside the bounds set by the tmHeight member. 
        Public tmInternalLeading As Integer

        ' The amount of extra leading (space) that the application adds between rows. 
        Public tmExternalLeading As Integer

        ' The average width of characters in the font (generally defined as the width of the letter x). 
        Public tmAveCharWidth As Integer

        ' The width of the widest character in the font. 
        Public tmMaxCharWidth As Integer

        ' The weight of the font. 
        Public tmWeight As Integer

        ' The extra width per string that may be added to some synthesized fonts. 
        Public tmOverhang As Integer

        ' The horizontal aspect of the device for which the font was designed. 
        Public tmDigitizedAspectX As Integer

        ' The vertical aspect of the device for which the font was designed. 
        Public tmDigitizedAspectY As Integer

        ' The value of the first character defined in the font. 
        Public tmFirstChar As Byte

        ' The value of the last character defined in the font. 
        Public tmLastChar As Byte

        ' The value of the character to be substituted for characters not in the font. 
        Public tmDefaultChar As Byte

        ' The value of the character that will be used to define word breaks for text justification. 
        Public tmBreakChar As Byte

        ' Specifies an italic font if it is nonzero. 
        Public tmItalic As Byte

        ' Specifies an underlined font if it is nonzero. 
        Public tmUnderlined As Byte

        ' A strikeout font if it is nonzero. 
        Public tmStruckOut As Byte

        ' Specifies information about the pitch, the technology, and the family of a physical font. 
        Public tmPitchAndFamily As Byte

        ' The character set of the font. 
        Public tmCharSet As Byte
    End Structure

#End Region

End Class


